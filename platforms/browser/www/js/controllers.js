angular.module('KushliDriver.controllers', [])

    .controller('LoginCtrl', function ($scope, LoginService, $rootScope, $state, $ionicPopup, $ionicLoading, $timeout, $http, $ionicHistory, $window, OrderModel) {
        $scope.data = {};

        $scope.showloading = function (){
			$ionicLoading.show({
				content: '<ion-sninner><ion-sninner>'
			});
		}		
		
		$scope.hideloading = function (){
			$ionicLoading.hide();
		}
		
		var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';

		$scope.signin = function() {
			$scope.showloading();
			var reg_id = window.localStorage.getItem("DEVICEREGIDPUSH");
			LoginService.signinfn($scope.data.useremail, $scope.data.password, reg_id, function(response) {                
				$scope.hideloading();
				if(response.success) {
					$http.defaults.headers.common['Authorization'] = 'Basic ' + response.auth_code; // jshint ignore:line
					window.localStorage.setItem(LOCAL_TOKEN_KEY, response.auth_code);
					$scope.TrackGeoLoc();
					$rootScope.tokenval = response.auth_code;
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true,
						historyRoot: true
					});
					$state.go('dashboard');
                } else {
                    if(!response.error)
						response.error = 'Issue in submitting form';
					var alertPopup = $ionicPopup.alert({
						title: 'Login Failed!',
						cssClass: 'my-custom-popup',
						template: response.error
					});
                }
				
            });
        }
		
		$scope.forgetpass = function() {
			$scope.showloading();
			LoginService.forgetpassfn($scope.data.forgetemail, function(response) {                
				$scope.hideloading();
				if(response.success) {
					var alertPopup = $ionicPopup.alert({
						template: response.success
					});
					$state.go('login');
                } else {
					var alertPopup = $ionicPopup.alert({
						template: response.error
					});
                }				
            });
        }
		
		$scope.TrackGeoLoc = function() {
			
			function onSuccess(position) {
			
				console.log('calling location function with order id'+position.coords.latitude+' '+position.coords.longitude);
				OrderModel.updateLoc(position.coords.latitude, position.coords.longitude, $rootScope.tokenval);
			}

			function onError(error) {
				//alert('code: '    + error.code    + '\n' +
				  //'message: ' + error.message + '\n');
			}

			var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 300000 });
			
			$rootScope.watchid = watchID;
		}
    })
	
    .controller('DashboardCtrl', function (OrderModel, $rootScope, $scope, $state, $ionicPopup, $ionicLoading, $stateParams, $http, $ionicHistory, $ionicPopover, $ionicSideMenuDelegate, $cordovaLaunchNavigator) {
		$scope.showloading = function (){
			$ionicLoading.show({
				content: '<ion-sninner><ion-sninner>'
			});
		}
		
		var template = '<ion-popover-view><ion-content><a>Account Info</a><a>Contact Dispensary</a><a ng-click="logout()">Logout</a></ion-content></ion-popover-view>';
		
		$scope.popover = $ionicPopover.fromTemplate(template, {
			scope: $scope, backdropClickToClose: true
		});
		
		$scope.popover.hide();
		
		$scope.togglePopover = function() {
			//$scope.popover.show($event);
			$ionicSideMenuDelegate.toggleRight();
		};

		$scope.navigatethis = function (destination) {
      		$cordovaLaunchNavigator.navigatethis(destination, {
        		enableDebug: true
      		}).then(function () {
        		alert("Navigator launched");
      		}, function (err) {
        		alert(err);
      		});
    	};
		
		var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';
		
		var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
		$rootScope.tokenval = token;
		
		//console.log('Token value in controller : '+$rootScope.tokenval);
		
		$scope.hideloading = function (){
			$ionicLoading.hide();
		}
		
		$scope.gotodashboard = function (){
			$scope.load_orders();
			$state.go('dashboard');
		}
		
		$scope.gotoassignments = function (){			
			$scope.load_assignments();
			$state.go('assignments');
		}
			
		$scope.my_profile = function() {
			$scope.showloading();
			console.log('My proile'+$rootScope.tokenval);
			OrderModel.getprofile($rootScope.tokenval, function(response) {                
				$scope.hideloading();
				console.log(JSON.stringify(response));
				if(response.success) {
                    $scope.myname = response.driver_info.name;
					$scope.myemail = response.driver_info.email;
					$scope.myphon_no = response.driver_info.phone_no;
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
                    $scope.msgclass = 'errormsg';
					$scope.error = response.error;
                }				
            });
        }
			
		$scope.load_orders = function() {
			$scope.ordmsgclass = '';
			$scope.orderror = '';
			$scope.showloading();
			$scope.porders = {};
			console.log('Token value in controller func : '+$rootScope.tokenval);
			OrderModel.AllOrders('allorders', $rootScope.tokenval, function(response) {                
				$scope.hideloading();
				console.log($scope.error);
				console.log(JSON.stringify(response));
				if(response.success) {
                    $scope.porders = response.orders;
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
						
					$scope.ordmsgclass = 'errormsg';
					$scope.orderror = response.error;
                    /*
					var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
					*/
                }
            });
        }
		
		$scope.load_refresh_orders = function() {
			$scope.ordmsgclass = '';
			$scope.orderror = '';
			$scope.porders = {};
			OrderModel.AllOrders('allorders', $rootScope.tokenval, function(response) {                
				$scope.$broadcast('scroll.refreshComplete');
				if(response.success) {
                    $scope.porders = response.orders;
                } else {
					if(!response.error)
						response.error = 'Problem in loading content';
                    $scope.ordmsgclass = 'errormsg';
					$scope.orderror = response.error;
                    /*
					var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
					*/
                }				
            });
        }
		
		$scope.load_assignments = function() {
			$scope.assmsgclass = '';
			$scope.asserror = '';
			$scope.passigns = {};
			$scope.showloading();
			OrderModel.AllAssignments('assignments', $rootScope.tokenval, function(response) {                
				$scope.hideloading();				
				if(response.success) {
                    $scope.passigns = response.orders;
                } else {
					if(!response.error)
						response.error = 'Problem in loading content';
                    $scope.assmsgclass = 'errormsg';
					$scope.asserror = response.error;
                    /*
					var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
					*/
                }
            });
        }
		
		$scope.load_refresh_assignments = function() {
			$scope.assmsgclass = '';
			$scope.asserror = '';
			$scope.passigns = {};
			OrderModel.AllAssignments('assignments', $rootScope.tokenval, function(response) {                
				$scope.$broadcast('scroll.refreshComplete');
				if(response.success) {
                    $scope.passigns = response.orders;
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
                    $scope.assmsgclass = 'errormsg';
					$scope.asserror = response.error;
                    /*
					var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
					*/
                }				
            });
        }		
		
		$scope.openlinkinbrowser = function(url) {
			console.log(url);
			window.open(url, '_system');
        }
		
		$scope.single_order = function() {
			$scope.showloading();
			var oid = $stateParams.oid;
			$scope.order = {};
			$scope.location_from = '';
			$scope.location_to = '';
			$scope.order_driver_status = '';
			OrderModel.singleOrder(oid, $rootScope.tokenval, function(response) {                
				$scope.hideloading();
				console.log(JSON.stringify(response));
				if(response.success) {
                    $scope.order = response.order;
					$scope.location_from = response.location_from;
					$scope.location_to = response.location_to;
					$scope.order_driver_status = response.order_driver_status;
					$scope.view_url = response.view_url;
					$scope.estimate_time = response.estimate_time;
					$scope.location_to_lat = response.location_to_lat
					$scope.location_to_lng = response.location_to_lng
					if(response.order_driver_status == "allocated"){
						$scope.estimate_status = 1;
					}else{
						$scope.estimate_status = response.estimate_status;
					}
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
                    $scope.msgclass = 'errormsg';
					$scope.error = response.error;
                    /*
					var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
					*/
                }				
            });
        }
		
		$scope.update_order_accept = function(oid) {
			$scope.showloading();
			OrderModel.orderaccept(oid, $rootScope.tokenval, function(response) {
				$scope.hideloading();			
				if(response.success) {
                    var alertPopup = $ionicPopup.alert({
						title: 'Success',
						template: 'Order accepted by driver.'
					});
					$scope.order_driver_status = "accepted";
					$scope.estimate_status = 0;
					/*
					if(ostatus == "accept"){
						$scope.trackoid = oid;
						$scope.TrackGeoLoc();
					}
					if(ostatus == "confirm" || ostatus == "failure"){
						$scope.ClearTrackingLoc();
					}
					*/
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
                    var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
                }				
            });
        }
		
		$scope.update_order_decline = function(oid) {
			$scope.data = {}
			var myPopup = $ionicPopup.show({
				template: '<div class="{{data.reasorerrclass}}">{{data.reasonerror}}</div><select ng-model="data.selreason"><option>My car is not working</option><option>I am not feeling well</option><option>This assignment is far away from my location</option><option>Other</option></select><input type="text" ng-show="data.selreason==\'Other\'" ng-model="data.otherreason" class="form-control">',
				title: 'Decline Reason',
				subTitle: '',
				scope: $scope,
				cssClass: 'my-custom-popup',
				buttons: [{
					text: 'Cancel'
				}, {
					text: '<b>Save</b>',
					type: 'button-positive',
					onTap: function(e) {
						if (!$scope.data.selreason) {
							$scope.data.reasorerrclass = 'errorlabel';
							$scope.data.reasonerror = 'Please select reason';
							e.preventDefault();
						} else if ($scope.data.selreason == "Other") {
							if(!$scope.data.otherreason){
								$scope.data.reasorerrclass = 'errorlabel';
								$scope.data.reasonerror = 'Please enter other reason';
								e.preventDefault();
							}else{
								return $scope.data;
							}
						} else {
							return $scope.data;
						}
					}
				}, ]
			});

			myPopup.then(function(res) {
				if (res) {
					$scope.showloading();
					var rejectreason = '';
					if (res.selreason == "Other") {
						if(res.otherreason){
							rejectreason = res.otherreason;
						}
					} else {
						rejectreason = res.selreason;
					}
					$scope.hideloading();
					
					OrderModel.orderreject(rejectreason, oid, $rootScope.tokenval, function(response) {
						$scope.hideloading();			
						if(response.success) {
							var alertPopup = $ionicPopup.alert({
								title: 'Success',
								template: 'Order rejected by driver.'
							});
							$scope.order_driver_status = "rejected";
						} else {
							if(!response.error)
								response.error = 'Problem in loading content';
							var alertPopup = $ionicPopup.alert({
								title: 'Error',
								template: response.error
							});
						}				
					});
				}
			});
        }
		
		$scope.update_order_confirm = function(oid) {
			$scope.showloading();
			OrderModel.orderconfirm(oid, $rootScope.tokenval, function(response) {
				$scope.hideloading();			
				if(response.success) {
                    var alertPopup = $ionicPopup.alert({
						title: 'Success',
						template: 'Order confirmed by driver.'
					});
					$scope.order_driver_status = "delivered";
					/*
					if(ostatus == "accept"){
						$scope.trackoid = oid;
						$scope.TrackGeoLoc();
					}
					if(ostatus == "confirm" || ostatus == "failure"){
						$scope.ClearTrackingLoc();
					}
					*/
                } else {
                    if(!response.error)
						response.error = 'Problem in loading content';
                    var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: response.error
					});
                }				
            });
        }
		
		$scope.update_order_failure = function(oid) {
			$scope.data = {}
			var myPopup = $ionicPopup.show({
				template: '<div class="{{data.reasorerrclass}}">{{data.reasonerror}}</div><select ng-model="data.selreason"><option>Location not found</option><option>Customer not available</option><option>Customer do not pick the phone</option><option>Other</option></select><input type="text" ng-show="data.selreason==\'Other\'" ng-model="data.otherreason" class="form-control">',
				title: 'Failure Reason',
				subTitle: '',
				scope: $scope,
				cssClass: 'my-custom-popup',
				buttons: [{
					text: 'Cancel'
				}, {
					text: '<b>Save</b>',
					type: 'button-positive',
					onTap: function(e) {
						if (!$scope.data.selreason) {
							$scope.data.reasorerrclass = 'errorlabel';
							$scope.data.reasonerror = 'Please select reason';
							e.preventDefault();
						} else if ($scope.data.selreason == "Other") {
							if(!$scope.data.otherreason){
								$scope.data.reasorerrclass = 'errorlabel';
								$scope.data.reasonerror = 'Please enter other reason';
								e.preventDefault();
							}else{
								return $scope.data;
							}
						} else {
							return $scope.data;
						}
					}
				}, ]
			});

			myPopup.then(function(res) {
				if (res) {
					$scope.showloading();
					var failerreason = '';
					if (res.selreason == "Other") {
						if(res.otherreason){
							failerreason = res.otherreason;
						}
					} else {
						failerreason = res.selreason;
					}
					OrderModel.orderfailer(failerreason, oid, $rootScope.tokenval, function(response) {
						$scope.hideloading();			
						if(response.success) {
							var alertPopup = $ionicPopup.alert({
								title: 'Success',
								template: 'Order failure by driver.'
							});
							$scope.order_driver_status = "failure";
						} else {
							if(!response.error)
								response.error = 'Problem in loading content';
							var alertPopup = $ionicPopup.alert({
								title: 'Error',
								template: response.error
							});
						}				
					});
				}
			});		
        }
		
		$scope.change_estimate_time = function(oid) {
			$scope.data = {estimatetimehr: '0', estimatetimemm: '01'}
			var myPopup = $ionicPopup.show({
				template: '<div class="{{data.reasorerrclass}}">{{data.reasonerror}}</div><span class="hours-list"> Hours <select class="form-control" ng-model="data.estimatetimehr"><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></span><span class="minutes-list"> Minutes <select class="form-control" ng-model="data.estimatetimemm"><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></span>',
				title: 'Change Estimate time',
				subTitle: '',
				scope: $scope,
				cssClass: 'my-custom-popup',
				buttons: [{
					text: 'Cancel'
				}, {
					text: '<b>Save</b>',
					type: 'button-positive',
					onTap: function(e) {
						if (!$scope.data.estimatetimehr || !$scope.data.estimatetimemm) {
							$scope.data.reasorerrclass = 'errorlabel';
							$scope.data.reasonerror = 'Please enter time';
							e.preventDefault();
						} else {
							return $scope.data;
						}
					}
				}, ]
			});

			myPopup.then(function(res) {
				if (res) {
					$scope.showloading();
					var estimatetimesent = '';
					if(res.estimatetimehr > 0){
						estimatetimesent = res.estimatetimehr+' hours';
					}
					if(res.estimatetimemm > 0){
						estimatetimesent = estimatetimesent+' '+res.estimatetimemm+' min';
					}
					OrderModel.orderestimatetime(estimatetimesent, oid, $rootScope.tokenval, function(response) {
						$scope.hideloading();	
						console.log('order id: '+oid+', '+JSON.stringify(response));
						if(response.success) {
							var alertPopup = $ionicPopup.alert({
								title: 'Success',
								template: 'Devlivery time successfully changed'
							});
							$scope.estimate_time = response.estimate_time;
							$scope.estimate_status = 1;
						} else {
							if(!response.error)
								response.error = 'Problem in loading content';
							var alertPopup = $ionicPopup.alert({
								title: 'Error',
								template: response.error
							});
						}
					});
				}
			});		
        }
		
		$scope.ClearTrackingLoc = function() {		
			navigator.geolocation.clearWatch($rootScope.watchid);		
		}
		
		$scope.logout = function() {
			$scope.popover.hide();
			$scope.showloading();
			OrderModel.signout($rootScope.tokenval, function(response) {
				$scope.hideloading();
				console.log(JSON.stringify(response));
				if(response.response) {
					localStorage.setItem(LOCAL_TOKEN_KEY, '');
					$http.defaults.headers.common.Authorization = 'Basic';
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true,
						historyRoot: true
					});
					$state.go('login');
				} else {
                    var alertPopup = $ionicPopup.alert({
						title: 'Error',
						template: 'Problem in signing you out'
					});
                }				
            });
		}
    })
	
	.controller('LogoutCtrl', function (OrderModel, $rootScope, $scope, $state, $ionicPopup, $ionicLoading, $stateParams, $http, $ionicHistory, $ionicPopover, $ionicSideMenuDelegate) {
		$scope.showloading = function (){
			$ionicLoading.show({
				content: '<ion-sninner><ion-sninner>'
			});
		}
		
		$scope.hideloading = function (){
			$ionicLoading.hide();
		}
		
		var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';
	
		$scope.showloading();
		OrderModel.signout($rootScope.tokenval, function(response) {
			$scope.hideloading();
			console.log(JSON.stringify(response));
			if(response.response) {
				localStorage.setItem(LOCAL_TOKEN_KEY, '');
				$http.defaults.headers.common.Authorization = 'Basic';
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: true
				});
				$state.go('login');
			} else {
				localStorage.setItem(LOCAL_TOKEN_KEY, '');
				$http.defaults.headers.common.Authorization = 'Basic';
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: true
				});
				$state.go('login');
			}				
		});
    });
	
	
	