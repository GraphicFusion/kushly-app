// Ionic template App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'KushliDriver' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('KushliDriver', ['ionic', 'KushliDriver.controllers', 'KushliDriver.services'])

    .run(function ($ionicPlatform, $rootScope) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
			
			if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                //StatusBar.styleLightContent();
            }
        });
    })
	
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
			.state('login', {
				cache: false,
                url: '/login',
                templateUrl: 'templates/tab-login.html',
                controller: 'LoginCtrl'
            })
			.state('profile', {
				cache: false,
                url: '/profile',
                templateUrl: 'templates/profile.html',
                controller: 'DashboardCtrl'
            })
			.state('dashboard', {
				cache: false,
                url: '/dashboard',
				templateUrl: 'templates/tab-dashboard.html',
                controller: 'DashboardCtrl'
            })
			.state('assignments', {
				cache: false,
                url: '/assignments',
				templateUrl: 'templates/tab-assignments.html',
                controller: 'DashboardCtrl'                
            })
            .state('orderdetail', {
				cache: false,
                url: '/orderdetail/:oid',
                templateUrl: 'templates/tab-orderdetail.html',
                controller: 'DashboardCtrl'
            })
			.state('assigndetail', {
                url: '/assigndetail',
                templateUrl: 'templates/tab-assigndetail.html',
                controller: 'DashboardCtrl'
            })
			.state('forgot_pass', {
                url: '/forgot_pass',
                templateUrl: 'templates/tab-forgot.html',
                controller: 'LoginCtrl'
            })
			.state('logout', {
				cache: false,
                url: '/logout',
				templateUrl: 'templates/tab-logout.html',
				controller: 'LogoutCtrl'
            })

			var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';
			var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
			if(token){
				$urlRouterProvider.otherwise('/dashboard');
			}else{
				$urlRouterProvider.otherwise('/login');
			}
    })

    .run(function ($rootScope, $state, $ionicLoading, $ionicHistory, OrderModel, $http) {
	
		$rootScope.$on('$stateChangeStart',function(event, toState){ 
			var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';
			var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
			if(token){
				if (toState.name == "login"){
					//console.log('This is login');
					$state.go('dashboard');					
				}
			}else{
				//console.log('Token not defined');
				if (toState.name != "login" && toState.name != "forgot_pass"){
					//console.log('This is login');
					$state.go('login');
				}
			}
		});
	
		$rootScope.$on('$stateChangeSuccess', function (event, toState) {
			var LOCAL_TOKEN_KEY = 'BASICAUTHCODE';
			var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
			if(token){
				if (toState.name == "login"){
					//console.log('This is login');
					$state.go('dashboard');					
				}
			}else{
				//console.log('Token not defined');
				if (toState.name != "login" && toState.name != "forgot_pass"){
					//console.log('This is login');
					$state.go('login');
				}
			}
        });		

    });