angular.module('KushliDriver.services', [])
    .service('OrderModel', function ($http) {
        var service = this;
		
		var APIURL = 'https://kushly.com';
		
		service.getprofile = function(token, callback) {
			$http.post(APIURL+'/api/v1/get-driver', {auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.AllOrders = function(ostatus, token, callback) {
			console.log('Token value in service : '+token);
			$http.post(APIURL+'/api/v1/orders', {ostatus: ostatus, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.AllAssignments = function(ostatus, token, callback) {
			$http.post(APIURL+'/api/v1/assignments', {ostatus: ostatus, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.singleOrder = function(orderid, token, callback) {
			$http.post(APIURL+'/api/v1/order', {order_id: orderid, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.orderaccept = function(orderid, token, callback) {
			$http.post(APIURL+'/api/v1/order/accept', {fsubmit: 'yes', order_id: orderid, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.orderreject = function(reason, orderid, token, callback) {
			$http.post(APIURL+'/api/v1/order/reject', {fsubmit: 'yes', order_id: orderid, rejection_reason: reason, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
				
		service.orderfailer = function(reason, orderid, token, callback) {
			$http.post(APIURL+'/api/v1/order/delivery/failer', {fsubmit: 'yes', order_id: orderid, failer_reason: reason, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.orderconfirm = function(orderid, token, callback) {
			$http.post(APIURL+'/api/v1/order/delivery/confirm', {fsubmit: 'yes', order_id: orderid, auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.updateLoc = function(lat, lng, token) {
			$http.post(APIURL+'/api/v1/driver/current/location', {fsubmit: 'yes', lat: lat, lng: lng, auth_code: token});
		}
		
		service.orderestimatetime = function(estimatetime, oid, token, callback) {
			$http.post(APIURL+'/api/v1/estimate-time', {auth_code: token, order_id: oid, estimate_time: estimatetime})
			.success(function (response) {
				callback(response);
			});
		}
		
		service.signout = function(token, callback) {
			$http.post(APIURL+'/api/v1/logout', {auth_code: token})
			.success(function (response) {
				callback(response);
			});
		}
    })
	
    .service('LoginService', function ($http, $rootScope) {
        var service = this;
		var APIURL = 'https://kushly.com';
		
		service.signinfn = function (email, password, firbaseregid, callback) {
			$http.post(APIURL+'/api/v1/login', { email: email, password: password, firbaseregid: firbaseregid })
			.success(function (response) {
				callback(response);
			});
        };
		
		service.forgetpassfn = function (email, callback) {
			$http.post(APIURL+'/api/v1/forget', { email: email })
			.success(function (response) {
				callback(response);
			});
        };
         	
    })

    //Map Launcher Plugin service
	.service('$cordovaLaunchNavigator', ['$q', function ($q) {
    "use strict";

    	var $cordovaLaunchNavigator = {};
    	$cordovaLaunchNavigator.navigatethis = function (destination, options) {
      		var q = $q.defer(),
      		isRealDevice = ionic.Platform.isWebView();
      		
      		if (!isRealDevice) {
        		q.reject("launchnavigator will only work on a real mobile device! It is a NATIVE app launcher.");
      		} else {
        		try {
          			var successFn = options.successCallBack || function () {
              			},
            			errorFn = options.errorCallback || function () {
              			},
            			_successFn = function () {
              				successFn();
              				q.resolve();
            			},
            			_errorFn = function (err) {
              				errorFn(err);
              				q.reject(err);
            			};

          			options.successCallBack = _successFn;
          			options.errorCallback = _errorFn;
          			launchnavigator.navigate(destination, options);
        		} catch (e) {
          			q.reject("Exception: " + e.message);
        		}
      		}
      		return q.promise;
    	};

    	return $cordovaLaunchNavigator;
  	}]);